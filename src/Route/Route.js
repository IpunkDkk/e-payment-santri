import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Bayar, Login, Splash, TopUp, Transaction} from '../Pages';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Image, View, Text} from 'react-native';
import {useEffect, useState} from 'react';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const Route = () => {
  const [splash, setSplash] = useState('true');
  useEffect(() => {
    setTimeout(() => {
      setSplash(false);
    }, 3000);
  }, []);
  return (
    <Stack.Navigator>
      {splash ? (
        <Stack.Screen
          name={'Splash'}
          component={Splash}
          options={{headerShown: false}}
        />
      ) : (
        <Stack.Group>
          <Stack.Screen
            name={'Login'}
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name={'Home'}
            component={TabNavigator}
            options={{
              headerTitle: props => <Header {...props} />,
              headerStyle: {
                backgroundColor: '#194826',
              },
              headerTintColor: 'white',
              headerTitleStyle: {
                fontWeight: 'bold',
              },
            }}
          />
        </Stack.Group>
      )}
    </Stack.Navigator>
  );
};

const TabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={() => ({
        tabBarActiveTintColor: '#194826',
      })}>
      <Tab.Screen
        name={'Transaksi'}
        component={Transaction}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Ionicons
              name="swap-horizontal-outline"
              color={color}
              size={size}
            />
          ),
        }}
      />
      <Tab.Screen
        name={'Bayar'}
        component={Bayar}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Ionicons name="qr-code-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name={'TopUp'}
        component={TopUp}
        options={{
          headerShown: false,
          tabBarIcon: ({color, size}) => (
            <Ionicons name="wallet-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const Header = () => {
  return (
    <View style={{flexDirection: 'row'}}>
      {/*<Image*/}
      {/*  style={{width: 40, height: 40}}*/}
      {/*  source={require('../assets/Logo/logonic.png')}*/}
      {/*/>*/}
      <View
        style={{
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            alignSelf: 'center',
            fontSize: 15,
            color: '#ffffff',
            marginLeft: 10,
          }}>
          e-Payment Santri
        </Text>
        <Text
          style={{
            fontSize: 10,
            color: '#ffffff',
            marginLeft: 10,
          }}>
          Nusantara IT Challange
        </Text>
      </View>
    </View>
  );
};

export default Route;
