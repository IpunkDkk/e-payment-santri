import {NavigationContainer} from '@react-navigation/native';
import Route from './Route/Route';

const App = () => {
  return (
    <NavigationContainer>
      <Route />
    </NavigationContainer>
  );
};

export default App;
