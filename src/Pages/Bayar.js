import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Alert,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {re} from '@babel/core/lib/vendor/import-meta-resolve';
import {LogBox} from 'react-native';
import ignoreWarnings from 'ignore-warnings';
import {TextInput} from 'react-native-paper';
ignoreWarnings('warn', ['ViewPropTypes', '[react-native-gesture-handler]']);

LogBox.ignoreLogs([
  "ViewPropTypes will be removed from React Native. Migrate to ViewPropTypes exported from 'deprecated-react-native-prop-types'.",
  'NativeBase: The contrast ratio of',
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
]);

function Bayar() {
  const [barcode, setBarcode] = useState(null);
  const handleScanner = async ress => {
    setBarcode(ress);
  };
  const handleDate = d => {
    let arr = [];
    let data = d.split('T');
    arr[0] = data[0];
    arr[1] = data[1].split('.')[0];
    return arr;
  };

  return (
    <View style={styles.screen}>
      <View style={styles.caption}>
        {barcode ? (
          <Text style={styles.captionTitleText}>
            Scanning Berhasil, Silahkan cek result
          </Text>
        ) : (
          <Text style={styles.captionTitleText}>
            Jangan bergerak saat melakukan scanning
          </Text>
        )}
      </View>

      {barcode ? (
        <View style={styles.rmCameraResult}>
          <Text style={styles.rmCameraResultText}>Menu Pembayaran</Text>
          <Image
            source={require('../assets/Logo/santri_icon.png')}
            style={{
              height: 100,
              width: 100,
              alignSelf: 'center',
              marginBottom: 20,
            }}
          />
          <Text style={styles.rmCameraResultText}>Trio Agung Purwanto</Text>
          <Text style={styles.rmCameraResultText}>2019020100074</Text>
          <TextInput
            label={'Bayar'}
            mode={'outlined'}
            style={{marginBottom: 10}}
            activeOutlineColor={'#194826'}
          />
          <TouchableOpacity style={styles.btn}>
            <Text style={styles.btnText}>Bayar</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <RNCamera
          style={styles.rnCamera}
          onBarCodeRead={ress => handleScanner(ress.data)}
          captureAudio={false}
          autoFocus={true}
        />
      )}

      <View style={styles.cameraControl}>
        <TouchableOpacity style={styles.btn2} onPress={() => setBarcode(null)}>
          <Text style={styles.btnText2}>Scan Lagi</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#F2F2FC',
  },
  saveArea: {
    backgroundColor: '#DCEFD3',
  },
  topBar: {
    height: 50,
    backgroundColor: '#DCEFD3',
    alignItems: 'center',
    justifyContent: 'center',
  },
  topBarTitleText: {
    color: 'black',
    fontSize: 20,
  },
  caption: {
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
  },
  captionTitleText: {
    color: '#121B0D',
    fontSize: 16,
    fontWeight: '600',
  },
  btn: {
    width: 200,
    alignSelf: 'center',
    borderRadius: 4,
    backgroundColor: '#194826',
    paddingHorizontal: 24,
    paddingVertical: 12,
    marginVertical: 8,
  },
  btn2: {
    width: 200,
    alignSelf: 'center',
    borderRadius: 4,
    backgroundColor: 'white',
    paddingHorizontal: 24,
    paddingVertical: 12,
    marginVertical: 8,
  },
  btnText: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
  btnText2: {
    fontSize: 18,
    color: '#194826',
    textAlign: 'center',
  },
  rnCamera: {
    flex: 1,
    width: '94%',
    alignSelf: 'center',
  },
  rmCameraResult: {
    backgroundColor: '#eeeeee',
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 10,
  },
  rmCameraResultText: {
    alignSelf: 'center',
    fontSize: 20,
    color: 'black',
  },
  cameraControl: {
    height: 180,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Bayar;
