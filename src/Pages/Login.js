import {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TextInput} from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

class Login extends Component {
  state = {
    email: '',
    password: '',
    login: '',
    secure: true,
  };
  emailText = inputText => {
    this.setState({email: inputText});
  };
  passwordText = inputText => {
    this.setState({password: inputText});
  };
  handleCLick = async () => {
    this.props.navigation.replace('Home');
  };
  render() {
    const navigation = this.props;
    return (
      <View style={style.container}>
        <View style={{width: '100%'}}>
          <Image
            source={require('../assets/baner/santri.png')}
            style={{height: 300, width: '100%'}}
          />
          <View style={{marginBottom: 30}}>
            <Text style={style.textHeader}>Welcome Back</Text>
            <Text style={style.text}>Sign In And Get Started</Text>
          </View>
          <View style={style.input}>
            <TextInput
              placeholder={'Email'}
              mode={'outlined'}
              onChangeText={this.emailText}
              activeOutlineColor={'#194826'}
              label={'Email'}
            />
          </View>
          <View style={style.input}>
            <TextInput
              secureTextEntry={this.state.secure}
              // style={styles.textInputStyle}
              right={
                <TextInput.Icon
                  name={() => (
                    <MaterialCommunityIcons
                      name={this.state.secure ? 'eye-off' : 'eye'}
                      size={28}
                      color={'black'}
                    />
                  )} // where <Icon /> is any component from vector-icons or anything else
                  onPress={() => {
                    this.state.secure
                      ? this.setState({secure: false})
                      : this.setState({secure: true});
                  }}
                />
              }
              placeholder={'Password'}
              underlineColorAndroid="transparent"
              label={'Password'}
              mode={'outlined'}
              onChangeText={this.passwordText}
              activeOutlineColor={'#194826'}
            />
          </View>
          <View style={style.forget}>
            <TouchableOpacity>
              <Text style={{fontSize: 12}}>Forget Password</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.handleCLick();
            }}>
            <View style={style.button}>
              <Text style={{color: 'white'}}>Sign In</Text>
            </View>
          </TouchableOpacity>

          <View style={{alignItems: 'center', marginTop: 12}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('SignUp');
              }}>
              <Text style={{fontSize: 12}}>Dont have an account? signup</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default Login;

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: '#194826',
    paddingVertical: 10,
    borderRadius: 5,
    marginTop: 20,
    alignItems: 'center',
  },
  input: {
    marginBottom: 6,
  },
  forget: {
    alignItems: 'flex-end',
  },
  textHeader: {
    color: '#194826',
    fontSize: 24,
    marginBottom: 10,
    fontFamily: 'Roboto',
  },
  text: {
    color: 'black',
    marginBottom: 30,
    fontFamily: 'Roboto',
  },
});
