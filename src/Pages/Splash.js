import {Image, StyleSheet, View} from 'react-native';

const Splash = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        source={require('../assets/Logo/logonic.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  logo: {
    height: 450,
    width: '100%',
  },
});

export default Splash;
