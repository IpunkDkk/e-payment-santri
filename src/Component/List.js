import {Component} from 'react';
import {FlatList, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const Data = [
  {id: 1, type: 'Pembayaran', nominal: '20000', date: '10:51 09 Januari 2022'},
  {id: 2, type: 'TopUp', nominal: '20000', date: '10:51 09 Januari 2022'},
  {id: 3, type: 'Pembayaran', nominal: '20000', date: '10:51 08 Januari 2022'},
  {id: 4, type: 'TopUp', nominal: '100000', date: '10:10 07 Januari 2022'},
  {id: 5, type: 'TopUp', nominal: '100000', date: '10:10 06 Januari 2022'},
  {id: 6, type: 'TopUp', nominal: '100000', date: '10:10 05 Januari 2022'},
  {id: 7, type: 'TopUp', nominal: '100000', date: '10:10 04 Januari 2022'},
  {id: 8, type: 'TopUp', nominal: '100000', date: '10:10 03 Januari 2022'},
];

class List extends Component {
  render() {
    return (
      <SafeAreaView style={styles.background}>
        <View style={styles.container}>
          <Text style={{color: 'black', fontSize: 16}}>List Transaksi</Text>
        </View>
        <FlatList
          data={Data}
          renderItem={({item}) => {
            return (
              <View style={styles.container}>
                <View style={styles.transaksi}>
                  <View
                    style={{
                      flex: 0.1,
                      borderWidth: 1,
                      padding: 4,
                      borderRadius: 4,
                      marginRight: 10,
                      backgroundColor: '#fef9f9',
                      borderColor: '#B0B0B4',
                    }}>
                    {item.type == 'Pembayaran' ? (
                      <MaterialCommunityIcons
                        name="account-arrow-up"
                        color="#194826"
                        size={30}
                      />
                    ) : (
                      <MaterialCommunityIcons
                        name="account-arrow-down"
                        color="#194826"
                        size={30}
                      />
                    )}
                  </View>
                  <View
                    style={{
                      flexDirection: 'column',
                      flex: 0.7,
                    }}>
                    <Text style={{color: 'black', fontSize: 16}}>
                      {item.type}
                    </Text>
                    <Text style={{color: 'black', fontSize: 11}}>
                      {item.date}
                    </Text>
                  </View>
                  <View style={{flex: 0.3}}>
                    {item.type == 'Pembayaran' ? (
                      <Text
                        style={{
                          color: 'black',
                          fontSize: 16,
                          fontWeight: 'bold',
                        }}>
                        - {item.nominal}
                      </Text>
                    ) : (
                      <Text
                        style={{
                          color: 'black',
                          fontSize: 16,
                          fontWeight: 'bold',
                        }}>
                        + {item.nominal}
                      </Text>
                    )}
                  </View>
                </View>
              </View>
            );
          }}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>
    );
  }
}

export default List;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    marginTop: 10,
    paddingHorizontal: 20,
  },
  transaksi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    borderTopWidth: 1,
    borderTopColor: '#B0B0B4',
    borderBottomWidth: 1,
    borderBottomColor: '#B0B0B4',
    paddingVertical: 10,
  },
});
